<?php

namespace Drupal\mandrill_inbound\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Creates an event.
 */
class MandrillInboundEvent extends Event {

  const MANDRILL_INBOUND_MESSAGE = 'mandrill_inbound.subscriber';

  protected $args;

  /**
   * {@inheritdoc}
   */
  public function __construct($args) {
    $this->args = $args;
  }

  /**
   * {@inheritdoc}
   */
  public function getArg() {
    return $this->args;
  }

}
