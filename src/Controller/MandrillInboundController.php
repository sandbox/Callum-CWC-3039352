<?php

namespace Drupal\mandrill_inbound\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization\Json;

/**
 * Menu callback for Inbound email via Mandrill.
 */
class MandrillInboundController extends ControllerBase {

  /**
   * Page callback for mandrill events.
   */
  public function page() {
    if ($_POST['mandrill_events'] != NULL) {
      $events = Json::decode($_POST['mandrill_events']);
      mandrill_inbound($events);
    }

    $element = [
      '#markup' => 'Mandrill Inbound',
    ];

    return $element;
  }

}
