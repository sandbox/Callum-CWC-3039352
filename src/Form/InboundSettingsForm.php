<?php

namespace Drupal\mandrill_inbound\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure site settings Mandrill Inbound.
 */
class InboundSettingsForm extends ConfigFormBase {
  const SETTINGS = 'mandrill_inbound.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mandrill_inbound_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form = [];

    $form['mandrill_inbound_email_domain'] = [
      '#type' => 'textfield',
      '#title' => t('Email domain'),
      '#default_value' => $config->get('mandrill_inbound_email_domain'),
    ];

    $form['current_webhook'] = [
      '#markup' => t('You should add follow webhook to routes: %url', [
        '%url' => \Drupal\Core\Url::fromRoute('mandrill_inbound.routing'),
      ]),
    ];

    $rows = [];
    foreach (\Drupal::service('mandrill.api')->getWebhooks() as $list) {
      $rows[] = [
        $list['id'],
        $list['url'],
        $list['auth_key'],
        isset($list['last_sent_at']) ? $list['last_sent_at'] : t('Never'),
        $list['batches_sent'],
        $list['events_sent'],
        $list['description'],
      ];
    }

    $header = [
      t('ID'),
      t('URL'),
      t('Auth Key'),
      t('Last Successful'),
      t('Batches Sent'),
      t('Events Sent'),
      t('Description'),
    ];

      $form['table'] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
      ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
      ->set('mandrill_inbound_email_domain', $form_state->getValue('mandrill_inbound_email_domain'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
