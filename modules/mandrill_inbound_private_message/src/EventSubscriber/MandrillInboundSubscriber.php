<?php

namespace Drupal\mandrill_inbound_private_message\EventSubscriber;

use Drupal\mandrill_inbound\Event\MandrillInboundEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * {@inheritdoc}
 */
class MandrillInboundSubscriber implements EventSubscriberInterface {

  /**
   * Get subscribed event.
   */
  public static function getSubscribedEvents() {
    $events[MandrillInboundEvent::MANDRILL_INBOUND_MESSAGE] = ['createPrivateMessage'];
    return $events;
  }

  /**
   * Creates the private message.
   */
  public function createPrivateMessage(MandrillInboundEvent $event) {

    $args = $event->getArg();
    mandrill_inbound_create_private_message($args);
  }

}
